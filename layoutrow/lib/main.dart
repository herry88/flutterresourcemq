import 'package:flutter/material.dart';

void main() {
  runApp(MainActivity());
}

class MainActivity extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Belajar Layout Row',
      home: new Scaffold(
        //membuat widget appbar
        appBar: new AppBar(
          title: new Text('Belajar Flutter'),
        ),
        body: new Center(
          child: new Row(
            //untuk mengisi ruang kosong pada layar
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              new Icon(
                Icons.add_a_photo,
                color: Colors.green,
                size: 40.0,
              ),
              new Icon(
                Icons.dashboard,
                color: Colors.red,
                size: 40.0,
              ),
              new Icon(
                Icons.access_alarm,
                color: Colors.blue,
                size: 40.0,
              )
            ],
          ),
        ),
      ),
    );
  }
}
