import 'package:flutter/material.dart';
import 'TextDemo.dart';
import 'IconsDemo.dart';
import 'ImagesDemo.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'value widgets',
      theme: ThemeData(
        primarySwatch: Colors.indigo,
      ),
      home: HomeScrene(),
    );
  }
}

class HomeScrene extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: const Text("value widgets"),
        ),
        body: TabBarView(
          children: <Widget>[
            TextDemo(),
            IconsDemo(),
            ImagesDemo(),
            // SimpleForm(),
            // ProperForm(),
          ],
        ),
        bottomNavigationBar: Material(
          color: Theme.of(context).colorScheme.primary,
          child: const TabBar(tabs: <Widget>[
            Tab(
              child: Text("TextPage"),
            ),
            Tab(
              child: Text("IconPage"),
            ),
            Tab(
              child: Text("ImagesPage"),
            ),
            // Tab(
            //   child: Text("SimplePage"),
            // ),
            // Tab(
            //   child: Text("ProperFormPage"),
            // )
          ]),
        ),
      ),
    );
  }
}
