import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Snacbar App',
      home: new Scaffold(
        appBar: AppBar(
          title: new Text('Snacbar App'),
        ),
        body: SnacbarPage(),
      ),
    );
  }
}

class SnacbarPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: RaisedButton(
        onPressed: () {
          final snackBar = SnackBar(
            content: new Text('Yes, ini adalah contoh snackbar'),
            action: SnackBarAction(
                label: 'Undo',
                onPressed: () {
                  //Some Code to undo the change
                }),
          );
          Scaffold.of(context).showSnackBar(snackBar);
        },
        child: new Text('Show Snackbar'),
      ),
    );
  }
}
