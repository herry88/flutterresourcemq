import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: 'Belajar Flutter',
    home: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Belajar Flutter"),
      ),
      body: Center(
          child: Image.network("https://img.icons8.com/all/500/flutter.png")),
    );
  }
}
